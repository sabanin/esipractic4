package demo.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.models.PlantHireRequest;

@Repository
public interface PlantHireRequestRepository extends JpaRepository<PlantHireRequest, Long> {

}
